<?php

/**
 * Cordial Sync
 * © 2017 Cordial Experiences, Inc. All rights reserved.
 **/
namespace Cordial\Sync\Model\Api;

use Laminas\Http\Request;
use Laminas\Http\Response;

class Client
{
    /**
     * API Key
     * @var string|null
     */
    protected $apiKey = null;

    /**
     * API URL
     * @var string|null
     */
    protected $apiUrl = null;

    protected $storeId = null;

    protected $client = null;

    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $jsonHelper;

    /**
     * @var \Cordial\Sync\Helper\Data
     */
    protected $helper = null;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \Cordial\Sync\Model\Log
     */
    protected $log;

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @param \Cordial\Sync\Helper\Data $helper
     * @param \Cordial\Sync\Model\Log $log
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        \Cordial\Sync\Helper\Data $helper,
        \Cordial\Sync\Model\Log $log,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Psr\Log\LoggerInterface $logger
    ) {

        $this->helper = $helper;
        $this->log = $log;
        $this->jsonHelper = $jsonHelper;
        $this->logger = $logger;
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    }

    public function load($storeId)
    {
        $this->storeId = $storeId;
        $this->apiKey = $this->helper->getApiKey($storeId);
        $this->apiUrl = $this->helper->getApiUrl($storeId);

        return $this;
    }

    protected function _getClient(): \Laminas\Http\Client
    {
        if (!$this->client instanceof \Laminas\Http\Client) {
            $this->client = new \Laminas\Http\Client();
        }
        return $this->client;
    }

    /**
     * Get API Key
     *
     * @return string
     */
    protected function _getApiKey()
    {
        return $this->apiKey;
    }

    /**
     * Get API URL
     *
     * @return string
     */
    protected function _getApiUrl()
    {
        return $this->apiUrl;
    }

    /**
     * Get API URI
     *
     * @return string
     */
    protected function getUri()
    {
        // v2.4.3-p2 2021-09-17
        if ($this->_getApiUrl()) {
            return Config::SCHEME . '://' . $this->_getApiUrl();
        }

        $cordialMode = getenv('CORDIAL_MODE', true) ?: getenv('CORDIAL_MODE');
        if ($cordialMode === 'dev') {
            return Config::SCHEME . '://' . Config::ENDPOINT_DEV;
        }

        return Config::SCHEME . '://' . Config::ENDPOINT;
    }

    /**
     * @param  string $method
     * @param  string $path
     * @param  array $options
     * @param  bool $needResponse
     * @link   http://api.cordial.io/docs/v2/
     * @return Response|Array|Mage_Exception
     */
    protected function _request($method = 'GET', $path = '', array $options = null, $needResponse = false)
    {
        try {
            $client = $this->_getClient();
            $client->resetParameters();
            $username = $this->_getApiKey();
            if (empty($username)) {
                throw new \Magento\Framework\Exception\LocalizedException(__('Requires the Cordial API key'));
            }

            $password = '';
            $client->setAuth($username, $password, \Laminas\Http\Client::AUTH_BASIC);
            $uri = $this->getUri();

            $client->setOptions(['timeout' => Config::TIMEOUT]);
            $client->setHeaders(
                [
                    'Accept' => 'application/json',
                    'Cache-Control' => 'no-cache',
                    'Accept-Encoding' => 'gzip,deflate'
                ]
            );
            $path = Config::VERSION . '/' . $path;

            $client->setUri($uri .'/'. $path);

            $client->setMethod($method);
            if (Request::METHOD_GET === $method) {
                $client->setParameterGet($options ?? []);
            } else {
                $client->setParameterPost($options ?? []);
            }

            $response = $client->send();
            $this->_save($response, $method, $path, $options);
        } catch (\Laminas\Http\Exception\ExceptionInterface $e) {
            $this->_save($e, $method, $path, $options);
            throw new \Magento\Framework\Exception\LocalizedException(__('Unable to Connect to Cordial API'));
        }

        if ($response->isServerError() || $response->isClientError()) {
            if ($response->getStatusCode() == '401' && $response->getReasonPhrase() == 'Unauthorized') {
                $this->logger->error("Cordial 401 err: {$method} {$uri}/{$path} " . $response->getMessage());
                $this->logger->error(var_export(array_keys($options), true) . " :: {$username}:{$password}");
                throw new \Magento\Framework\Exception\LocalizedException(__('Unable to Connect 401 to Cordial API'));
            }
            return false;
        }

        if ($needResponse) {
            return $this->jsonHelper->jsonDecode($response->getBody());
        }

        if ($method == 'GET') {
            return $this->jsonHelper->jsonDecode($response->getBody());
        }
        return true;
    }

    /**
     * Save log
     */
    protected function _save($response, $method = 'GET', $path = '', array $options = null)
    {
        $data = [];
        $data['method'] = $method;
        $data['path'] = $path;
        $data['options'] = $this->jsonHelper->jsonEncode($options);

        if ($response instanceof Response) {
            $data['message'] = $response->getReasonPhrase();
            $data['body'] = $response->getBody();
            $data['code'] = $response->getStatusCode();
        }

        $log = $this->log->setData($data);

        try {
            $log->save();
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
        }
    }
}
