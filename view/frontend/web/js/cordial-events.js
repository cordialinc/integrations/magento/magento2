define(['uiComponent', 'Magento_Customer/js/customer-data', 'ko'], function(Component, customerData, ko) {
    return Component.extend({
        events: ko.observable({data: ''}),

        initialize: function () {
            this._super();
            var self = this;
            customerData.get('cordial').subscribe(function (events) {
                self.events({data: events.data});
            });
        }
    });
});
